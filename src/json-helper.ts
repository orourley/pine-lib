import type { JsonType, JsonArray, JsonObject } from './json-type';

export class JsonHelper {

	static isNumber(json: unknown): json is number {
		return typeof json === 'number';
	}

	static isString(json: unknown): json is string {
		return typeof json === 'string';
	}

	static isBoolean(json: unknown): json is boolean {
		return typeof json === 'boolean';
	}

	static isObject(json: unknown): json is JsonObject {
		return typeof json === 'object' && !JsonHelper.isArray(json) && json !== null;
	}

	static isArray(json: unknown): json is JsonArray {
		return Array.isArray(json);
	}

	static isObjectOfStrings(json: unknown): json is Record<string, string> {
		if (!JsonHelper.isObject(json)) {
			return false;
		}
		for (const key of Object.keys(json)) {
			if (typeof json[key] !== 'string') {
				return false;
			}
		}
		return true;
	}

	static isObjectOfNumbers(json: unknown): json is Record<string, number> {
		if (!JsonHelper.isObject(json)) {
			return false;
		}
		for (const key of Object.keys(json)) {
			if (typeof json[key] !== 'number') {
				return false;
			}
		}
		return true;
	}

	static isObjectOfV<V extends JsonType>(json: unknown, isVFunction: (value: unknown) => value is V): json is Record<string, V> {
		if (!JsonHelper.isObject(json)) {
			return false;
		}
		for (const key of Object.keys(json)) {
			if (!isVFunction(json[key])) {
				return false;
			}
		}
		return true;
	}

	static isArrayOfStrings(json: unknown): json is string[] {
		if (!JsonHelper.isArray(json)) {
			return false;
		}
		for (let i = 0, l = json.length; i < l; i++) {
			if (typeof json[i] !== 'string') {
				return false;
			}
		}
		return true;
	}

	static isArrayOfNumbers(json: unknown): json is number[] {
		if (!JsonHelper.isArray(json)) {
			return false;
		}
		for (let i = 0, l = json.length; i < l; i++) {
			if (typeof json[i] !== 'number') {
				return false;
			}
		}
		return true;
	}

	static isArrayOfBooleans(json: unknown): json is boolean[] {
		if (!JsonHelper.isArray(json)) {
			return false;
		}
		for (let i = 0, l = json.length; i < l; i++) {
			if (typeof json[i] !== 'boolean') {
				return false;
			}
		}
		return true;
	}

	static isArrayOfV<V extends JsonObject | JsonArray>(json: unknown, isVFunction: (value: unknown) => value is V): json is V[] {
		if (!JsonHelper.isArray(json)) {
			return false;
		}
		for (let i = 0, l = json.length; i < l; i++) {
			if (!isVFunction(json[i])) {
				return false;
			}
		}
		return true;
	}

	// Returns true if json[key] is a number.
	static hasNumberProp<K extends string>(json: JsonObject, key: K): json is JsonObject & Record<K, number> {
		return typeof json[key] === 'number';
	}

	// Returns true if json[key] is a string.
	static hasStringProp<K extends string>(json: JsonObject, key: K): json is JsonObject & Record<K, string> {
		return typeof json[key] === 'string';
	}

	// Returns true if json[key] is a boolean.
	static hasBooleanProp<K extends string>(json: JsonObject, key: K): json is JsonObject & Record<K, boolean> {
		return typeof json[key] === 'boolean';
	}

	// Returns true if json[key] is a T.
	static hasProp<K extends string, T>(json: JsonObject, key: K, isFunc: (json: unknown) => json is T): json is JsonObject & Record<K, T> {
		return isFunc(json[key]);
	}

	/** Deep clones JSON. Taken from https://stackoverflow.com/a/17502990. */
	static cloneJSON<T extends JsonType>(json: T): T {

		// Array copy.
		if (JsonHelper.isArray(json)) {
			const clone: JsonArray = [];
			for (let i = 0, l = json.length; i < l; i++) {
				const value = json[i];
				if (value !== undefined) {
					clone[i] = this.cloneJSON(value);
				}
			}
			return clone as T;
		}

		// Object copy
		else if (JsonHelper.isObject(json)) {
			const clone: JsonObject = {};
			for (const [key, value] of Object.entries(json)) {
				if (value !== undefined) {
					clone[key] = this.cloneJSON(value);
				}
			}
			return clone as T;
		}

		// Primitive or null
		else {
			return json;
		}
	}

	/** Converts a class to a JsonObject. */
	static objectToJsonObject(object: Record<string, unknown>): JsonObject {
		const r: JsonObject = {};
		for (const [key, value] of Object.entries(object)) {
			if (JsonHelper.isString(value) || JsonHelper.isNumber(value) || JsonHelper.isBoolean(value) || value === null) {
				r[key] = value;
			}
			else if (JsonHelper.isObject(value)) {
				r[key] = this.objectToJsonObject(value);
			}
		}
		return {};
	}
}
