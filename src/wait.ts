/** Waits some seconds and then returns the promise. */
export async function wait(delay: number): Promise<void> {
	return new Promise<void>((resolve) => {
		setTimeout(() => {
			resolve();
		}, delay * 1000);
	});
}

/** Waits until the testFunc returns true.
 *  @param testFunc - The testing function. Returning true stops the wait.
 *  @param testInterval - The interval in seconds to wait between tests.
 *  @param timeout - The time at which point the promise rejects. */
export async function waitUntil(testFunc: () => boolean, testInterval: number, timeout: number): Promise<void> {
	return new Promise((resolve, reject) => {
		let timeSoFar = 0;
		const intervalCheck = setInterval(() => {

			// Do the test.
			if (testFunc()) {
				clearInterval(intervalCheck);
				resolve();
			}

			// Increase the time we've waited.
			timeSoFar += testInterval;

			// If we've hit the timeout, reject.
			if (timeSoFar >= timeout) {
				clearInterval(intervalCheck);
				reject(new Error());
			}
		}, testInterval * 1000.0);
	});
}
