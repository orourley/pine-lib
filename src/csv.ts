export class Csv {

	/** Parse a CSV text. From https://stackoverflow.com/a/14991797. */
	static parse(text: string, delim: string = ','): string[][] {

		/** The table to be returned. */
		const table: string[][] = [];

		/** Whether or not we're in a quoted field. */
		let quotedField = false;

		// Iterate over each character, keep track of current row and column of the returned table.
		let rowI = 0;
		let colI = 0;
		for (let i = 0, l = text.length; i < l; i++) {

			/** The current character. */
			const c = text[i]!;

			/** The next character. */
			const cNext: string | undefined = text[i + 1];

			// Create a new row if necessary.
			table[rowI] ??= [];
			const row = table[rowI]!;

			// Create a new cell if necessary.
			row[colI] ??= '';

			// If the current and next characters are quotation marks and we're inside a quoted field,
			// add a quotation mark to the current column and skip the next character.
			if (c === '"' && cNext === '"' && quotedField) {
				row[colI] += c;
				i++;
			}

			// If it's just one quotation mark, begin/end quoted field
			else if (c === '"') {
				quotedField = !quotedField;
			}

			// If it's a comma and we're not in a quoted field, move on to the next column.
			else if (c === delim && !quotedField) {
				colI++;
			}

			// If it's a carriage return or lone newline and we're not in a quoted field,
			// move on to the next row and move to column 0 of that new row.
			else if ((c === '\n' || c === '\r') && !quotedField) {
				rowI++;
				colI = 0;

				// If it's a carriage return and newline and we're not in a quoted field, skip the next character.
				if (c === '\r' && cNext === '\n') {
					i++;
				}
			}

			// Otherwise, append the current character to the current column.
			else {
				row[colI] += c;
			}
		}

		// Return the table.
		return table;
	}
}
