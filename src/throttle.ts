/** A throttle class that calls a callback. If the callback has been called recently, it is defered until the interval time. */
export class Throttle {

	/** The minimum time between calls, in seconds. */
	interval: number;

	/** Constructs the class. */
	constructor(callback: () => void, interval: number) {
		this._callback = callback;
		this.interval = interval;
	}

	/** Destroys the class, cancelling any pending call. */
	destroy() {
		clearTimeout(this._timeout);
	}

	/** Calls the callback either immediately, if the required interval has passed, or delayed until the interval has passed. */
	call() {
		if (this._timeout === undefined) {
			const time = Date.now() / 1000;
			const delay = Math.max(0, this._lastCallTime + this.interval - time);
			this._timeout = setTimeout(this._callImmediately.bind(this, time + delay), 1000 * delay);
		}
	}

	/** Cancels any pending call and clears the last call time. */
	cancelCall() {
		this._lastCallTime = Number.NEGATIVE_INFINITY;
		clearTimeout(this._timeout);
		this._timeout = undefined;
	}

	/** Calls the callback, noting the time. */
	private _callImmediately(time: number) {
		this._lastCallTime = time;
		this._timeout = undefined;
		this._callback();
	}

	/** The callback to call, in */
	private _callback: () => void;

	/** The time of the last call. */
	private _lastCallTime: number = Number.NEGATIVE_INFINITY;

	/** The timeout for calling the callback. */
	private _timeout: ReturnType<typeof setTimeout> | undefined;
}
