/** A generator of ids that are unique per instance of the class. */
export class UniqueIds {

	/** Gets a unique id. */
	get(): number {
		let id = this._freeIds.pop();
		if (id === undefined) {
			id = this._numUsed;
		}
		this._numUsed += 1;
		return id;
	}

	/** Releases a previously gotten id. */
	release(id: number) {
		this._freeIds.push(id);
		this._numUsed -= 1;
	}

	/** The free ids. If there are no free ids, the next free id is the number used. */
	private _freeIds: number[] = [];

	/** The number of used ids. */
	private _numUsed: number = 0;
}
