import { MathHelper } from './math-helper';

/** A binary data writer. */
export class Writer {

	/** Constructor. */
	constructor() {
		this._array = new Uint8Array(8);
		this._dataView = new DataView(this._array.buffer);
		this._length = 0;
		this._textEncoder = new TextEncoder();
	}

	/** Gets the data array. */
	getData(): Uint8Array {
		// First make sure it is the exact length.
		if (this._length !== this._array.length) {
			this._array = this._array.slice(0, this._length);
			this._dataView = new DataView(this._array.buffer);
		}
		return this._array;
	}

	/** Writes an 8-bit unsigned integer. */
	writeUInt8(v: number) {
		this.checkGrow(1);
		this._dataView.setUint8(this._length, v);
		this._length += 1;
	}

	/** Writes a 16-bit unsigned integer. */
	writeUInt16(v: number) {
		this.checkGrow(2);
		this._dataView.setUint16(this._length, v, true);
		this._length += 2;
	}

	/** Writes a 32-bit unsigned integer. */
	writeUInt32(v: number) {
		this.checkGrow(4);
		this._dataView.setUint32(this._length, v, true);
		this._length += 4;
	}

	/** Writes a 64-bit unsigned integer. */
	writeUInt64(v: number) {
		this.checkGrow(8);
		this._dataView.setBigUint64(this._length, BigInt(v), true);
		this._length += 8;
	}

	/** Writes an 8-bit signed integer. */
	writeInt8(v: number) {
		this.checkGrow(1);
		this._dataView.setInt8(this._length, v);
		this._length += 1;
	}

	/** Writes a 16-bit signed integer. */
	writeInt16(v: number) {
		this.checkGrow(2);
		this._dataView.setInt16(this._length, v, true);
		this._length += 2;
	}

	/** Writes a 32-bit signed integer. */
	writeInt32(v: number) {
		this.checkGrow(4);
		this._dataView.setInt32(this._length, v, true);
		this._length += 4;
	}

	/** Writes a 64-bit signed integer. */
	writeInt64(v: number) {
		this.checkGrow(8);
		this._dataView.setBigInt64(this._length, BigInt(v), true);
		this._length += 8;
	}

	/** Writes a 32-bit floating-point number. */
	writeFloat32(v: number) {
		this.checkGrow(4);
		this._dataView.setFloat32(this._length, v, true);
		this._length += 4;
	}

	/** Writes a 64-bit floating-point number. */
	writeFloat64(v: number) {
		this.checkGrow(8);
		this._dataView.setFloat64(this._length, v, true);
		this._length += 8;
	}

	/** Writes a string. It can have the length prefixed as a u32 or postfixed in a u8 zero. */
	writeString(v: string, endsWithZero: boolean) {
		const strBytes = this._textEncoder.encode(v);
		if (!endsWithZero) {
			this.writeUInt32(strBytes.length);
		}
		this.checkGrow(strBytes.length);
		for (let i = 0, l = strBytes.length; i < l; i++) {
			this._dataView.setUint8(this._length + i, strBytes[i]!);
		}
		this._length += strBytes.length;
		if (endsWithZero) {
			this.writeUInt8(0);
		}
	}

	/** Writes an array with a prepended u32 length. */
	writeArray<T>(v: T[], writeItemFunction: (writer: Writer, item: T) => void) {
		this.writeUInt32(v.length);
		for (let i = 0, l = v.length; i < l; i++) {
			writeItemFunction(this, v[i]!);
		}
	}

	/** Writes an array buffer. */
	writeArrayBuffer(v: ArrayBufferLike) {
		this.checkGrow(v.byteLength);
		this._array.set(new Uint8Array(v), this._length);
		this._length += v.byteLength;
	}

	/** Checks to see if the inner buffer needs to grow or not, and does so. */
	private checkGrow(numBytes: number) {
		if (this._length + numBytes > this._dataView.buffer.byteLength) {
			const newBuffer = new Uint8Array(new ArrayBuffer(Math.max(64, MathHelper.nextPowOf2(this._length + numBytes))));
			newBuffer.set(this._array);
			this._array = newBuffer;
			this._dataView = new DataView(this._array.buffer);
		}
	}

	/** The array of data. */
	private _array: Uint8Array<ArrayBuffer>;

	/** The data view for writing the data. */
	private _dataView: DataView;

	/** The position in the array to next write. */
	private _length: number;

	/** The text encoder for writing UTF-8 strings. */
	private _textEncoder: TextEncoder;
}
