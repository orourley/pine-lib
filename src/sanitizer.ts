export class Sanitizer {

	/** Converts a string into one suitable for HTML display. */
	static sanitizeForHtml(s: string): string {
		return s.replace(/&/gu, '&amp;').replace(/</gu, '&lt;');
	}

	/** Converts an HTML string back into its original string. */
	static desanitizeFromHtml(s: string): string {
		return s.replace(/&lt;/gu, '<').replace(/&amp;/gu, '&');
	}

	/** Converts a string into one suitable for an callback attribute. */
	static sanitizeForCallbackAttribute(s: string): string {
		return this.sanitizeForAttribute(s).replace(/\|/gu, '\\|');
	}

	/** Converts a callback attribute string back into its original string. */
	static desanitizeFromCallbackAttribute(s: string): string {
		return this.desanitizeFromAttribute(s.replace(/\\\|/gu, '|'));
	}

	/** Converts a string into one suitable for an attribute. */
	static sanitizeForAttribute(s: string): string {
		return s.replace(/&/gu, '&amp;').replace(/"/gu, '&quot;');
	}

	/** Converts an attribute string back into its original string. */
	static desanitizeFromAttribute(s: string): string {
		return s.replace(/&quot;/gu, '"').replace(/&amp;/gu, '&');
	}

	/** Converts a string into one suitable for a url query param. */
	static sanitizeForUrlQueryParam(s: string): string {
		return encodeURIComponent(s);
	}

	/** Converts a url query param back into its original string. */
	static desanitizeFromUrlQueryParam(s: string): string {
		return decodeURIComponent(s);
	}
}
