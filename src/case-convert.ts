/** Functions for converting cases. */
export class CaseConvert {

	/** Converts a CamelCase to a kebab-case. */
	static convertCamelCaseToKebabCase(camelCase: string, underscore?: boolean): string {
		let kebabCase = '';
		for (let i = 0, l = camelCase.length; i < l; i++) {
			kebabCase += camelCase[i]!.toLowerCase();
			if (camelCase[i]! >= 'a' && camelCase[i]! <= 'z' && i < camelCase.length - 1 && camelCase[i + 1]! >= 'A' && camelCase[i + 1]! <= 'Z') {
				kebabCase += underscore === true ? '_' : '-';
			}
		}
		return kebabCase;
	}

	/** Converts a CamelCase to a kebab-case. */
	static convertKebabCaseToCamelCase(kebabCase: string): string {
		let camelCase = '';
		for (let i = 0, l = kebabCase.length; i < l; i++) {
			if (kebabCase[i] === '-' || kebabCase[i] === '_') {
				i += 1;
				if (i < kebabCase.length) {
					camelCase += kebabCase[i]!.toUpperCase();
					i += 1;
				}
			}
			else {
				camelCase += kebabCase[i];
			}
		}
		return camelCase;
	}
}
