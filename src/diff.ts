/** Functions for getting the diff of arrays, sets, objects, etc. */
export class Diff {

	/** Given old and new arrays, populate the added and removed arrays. */
	static getArrayAddsAndRemoves<T>(oldArray: readonly T[], newArray: readonly T[], addedArray: T[], removedArray: T[]) {

		// Add to the added array any values that are new.
		for (let i = 0; i < newArray.length; i++) {
			if (!oldArray.includes(newArray[i]!)) {
				addedArray.push(newArray[i]!);
			}
		}

		// Add to the removed array any values that are old.
		for (let i = 0; i < oldArray.length; i++) {
			if (!newArray.includes(oldArray[i]!)) {
				removedArray.push(oldArray[i]!);
			}
		}
	}

	/** Given old and new sets, populate the added and removed sets. */
	static getSetAddsAndRemoves<T>(oldSet: Set<T>, newSet: Set<T>, addedSet: Set<T>, removedSet: Set<T>) {

		// Add to the added set any values that are new.
		for (const newValue of newSet) {
			if (!oldSet.has(newValue)) {
				addedSet.add(newValue);
			}
		}

		// Add to the removed set any values that are old.
		for (const oldValue of oldSet) {
			if (!newSet.has(oldValue)) {
				removedSet.add(oldValue);
			}
		}
	}

	/** Given old and new objects, populate the addedObject and removed objects. */
	static getObjectAddsAndRemoves<T>(oldObject: Record<string, T>, newObject: Record<string, T>,
		addedObject: Record<string, T>, removedObject: Record<string, T>) {

		// Add to the addedOrChangedObject object any properties that are new or changed (also to removed).
		for (const [newKey, newValue] of Object.entries(newObject)) {
			if (newKey in oldObject) {
				// The property changed its value, so set it as both removed and added.
				if (oldObject[newKey] !== newValue) {
					removedObject[newKey] = oldObject[newKey]!;
					addedObject[newKey] = newValue;
				}
			}
			// The param didn't already exist, so set it as added.
			else {
				addedObject[newKey] = newValue;
			}
		}

		// Add to the removed object any properties that are no longer in the new object.
		for (const [oldKey, oldValue] of Object.entries(oldObject)) {
			if (!(oldKey in newObject)) {
				removedObject[oldKey] = oldValue;
			}
		}
	}
}
