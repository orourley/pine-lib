/** A JSON object. */
export type JsonObject = { [key: string]: JsonType | undefined; };

/** A JSON array. */
export type JsonArray = JsonType[];

/** A JSON type. */
export type JsonType = null | boolean | number | string | JsonArray | JsonObject;
