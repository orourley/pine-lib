/** A library of array sorting functions. */
export class Sort {

	/** Gets the least index of the sorted array that is greater than or equal to the key, or the
	 *  length if all keys are less. O(log n). */
	static getIndex<ValueType, KeyType>(key: KeyType, array: ValueType[], isLess: (lhs: ValueType, rhs: KeyType) => boolean): number {
		let low = 0;
		let high = array.length;
		while (low < high) {
			const mid = Math.floor((low + high) / 2);
			if (isLess(array[mid]!, key)) {
				low = mid + 1;
			}
			else {
				high = mid;
			}
		}
		return low;
	}

	/** Returns true if the key was found in the sorted array. */
	static has<ValueType, KeyType>(key: KeyType, array: ValueType[], isLess: (lhs: ValueType, rhs: KeyType) => boolean,
		isEqual: (lhs: ValueType, rhs: KeyType) => boolean): boolean {
		const index = this.getIndex(key, array, isLess);
		if (index < array.length && isEqual(array[index]!, key)) {
			return true;
		}
		return false;
	}

	/** Adds the value into the sorted array. Returns the index where it was added. */
	static add<ValueType>(value: NoInfer<ValueType>, array: ValueType[], isLess: (lhs: ValueType, rhs: ValueType) => boolean): number {
		const index = this.getIndex(value, array, isLess);
		array.splice(index, 0, value);
		return index;
	}

	/** Adds the value into the sorted array, if it hasn't already been added. Returns the index if was added, or undefined if not. */
	static addIfUnique<ValueType>(value: NoInfer<ValueType>, array: ValueType[], isLess: (lhs: ValueType, rhs: ValueType) => boolean,
		isEqual: (lhs: ValueType, rhs: ValueType) => boolean): number | undefined {
		const index = this.getIndex(value, array, isLess);
		if (index < array.length && isEqual(array[index]!, value)) {
			return undefined;
		}
		array.splice(index, 0, value);
		return index;
	}

	/** Adds the value into the sorted array, overwriting any existing value. Returns the index where it was added. */
	static addOrOverwrite<ValueType>(value: NoInfer<ValueType>, array: ValueType[], isLess: (lhs: ValueType, rhs: ValueType) => boolean,
		isEqual: (lhs: ValueType, rhs: ValueType) => boolean): number {
		const index = this.getIndex(value, array, isLess);
		if (index < array.length && isEqual(array[index]!, value)) {
			array.splice(index, 1, value);
			return index;
		}
		array.splice(index, 0, value);
		return index;
	}

	/** Removes the key's value from the sorted array. Returns the index if the key was found, otherwise undefined. */
	static remove<ValueType, KeyType>(key: KeyType, array: ValueType[], isLess: (lhs: ValueType, rhs: KeyType) => boolean,
		isEqual: (lhs: ValueType, rhs: KeyType) => boolean): number | undefined {
		const index = this.getIndex(key, array, isLess);
		if (index < array.length && isEqual(array[index]!, key)) {
			array.splice(index, 1);
			return index;
		}
		return undefined;
	}

	/** Sorts the values in the array based on the *isLess* function. Uses insertion sort. */
	static sort<ValueType>(array: ValueType[], isLess: (lhs: ValueType, rhs: ValueType) => boolean,
		onSwap?: (array: ValueType[], i: number, j: number) => void) {
		if (array.length === 0) {
			return;
		}
		let n = 1;
		while (n < array.length) {
			let m = n - 1;
			while (m >= 0 && isLess(array[m + 1]!, array[m]!)) {
				const t = array[m]!;
				array[m] = array[m + 1]!;
				array[m + 1] = t;
				onSwap?.(array, m, m + 1);
				m--;
			}
			n++;
		}
	}
}
