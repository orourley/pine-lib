/** Collection of math helper functions. */
export class MathHelper {

	/** Gets the ceiling power of two. */
	static nextPowOf2(v: number): number {
		if (v > 0) {
			return 2 ** Math.ceil(Math.log2(v));
		}
		else {
			return 0;
		}
	}

	/** Rounds a number to the nearest number of decimals. */
	static round(v: number, decimals: number): number {
		const mult = 10 ** decimals;
		return Math.round((v + Number.EPSILON) * mult) / mult;
	}
}
