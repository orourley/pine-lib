/** A lockable mutex. It locks in order of requests. To use it, do:
 * ```
 * const mutex = new Mutex();
 * const unlock = await mutex.lock();
 * // Your code here.
 * unlock();
 * ``` */
export class Mutex {

	/** Returns a Promise that waits until there is no other lock, and then resolves to an unlock function, which releases the lock. */
	async lock(): Promise<() => void> {
		let resolve: () => void;
		const promise = new Promise<void>((_resolve) => {
			resolve = _resolve;
		});
		const unlock = this.promise.then(() => resolve);
		this.promise = promise;
		return unlock;
	}

	/** The latest promise that has requested a lock (either waiting or resolved). */
	private promise: Promise<void> = Promise.resolve();
}

/** For testing:

const rand = (max: number): number => Math.floor(Math.random() * max);
const delay = (ms: number): Promise<void> => new Promise(resolve => setTimeout(resolve, ms));
const lock = new Mutex();
const go = async (name: string): Promise<void> => {
	await delay(rand(50));
	console.log(name + ' requesting lock');
	const unlock = await lock.lock();
	console.log(name + ' got lock');
	await delay(rand(1000));
	console.log(name + ' releasing lock');
	unlock();
};
go('0');
go('1');
go('2');
go('3');
go('4');
go('5');
go('6');
go('7');
go('8');
go('9');

*/
