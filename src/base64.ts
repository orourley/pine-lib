export class Base64 {

	/** Converts the array buffer into a base-64 string. */
	static toBase64(buffer: ArrayBuffer | SharedArrayBuffer): string {
		let binary = '';
		const bytes = new Uint8Array(buffer);
		for (let i = 0, l = bytes.length; i < l; i++) {
			binary += String.fromCharCode(bytes[i]!);
		}
		return btoa(binary);
	}

	/** Converts the base-64 string to an array buffer. */
	static toArrayBuffer(data: string): ArrayBuffer {
		const binaryString = atob(data);
		const bytes = new Uint8Array(binaryString.length);
		for (let i = 0; i < binaryString.length; i++) {
			bytes[i] = binaryString.charCodeAt(i);
		}
		return bytes.buffer;
	}
}
