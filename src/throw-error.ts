/** A function that simply throws the Error(message).
 *  Needed when using `const a = expressionMaybeUndefined ?? throw new Error(message);`,
 *  because TypeScript doesn't yet support throw expressions. */
export function throwError(message: string): never {
	throw new Error(message);
}
