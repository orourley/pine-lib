/** A map that is O(1) in both directions. */
export class Bimap<K, V> {

	/** The constructor. */
	constructor(iterable?: Iterable<readonly [K, V]>) {

		// Set the iterable entries.
		if (iterable) {
			for (const [key, value] of iterable) {
				this._keyMap.set(key, value);
				this._valueMap.set(value, key);
			}
		}
	}

	// ACCESS

	/** Gets the value from the key, or undefined if the key does not exist. */
	getValue(key: K): V | undefined {
		return this._keyMap.get(key);
	}

	/** Gets the key from the value, or undefined if the value does not exist. */
	getKey(value: V): K | undefined {
		return this._valueMap.get(value);
	}

	/** Returns true if the key exists. */
	hasKey(key: K): boolean {
		return this._keyMap.has(key);
	}

	/** Returns true if the value exists. */
	hasValue(value: V): boolean {
		return this._valueMap.has(value);
	}

	/** Gets the number of entries. */
	getSize(): number {
		return this._keyMap.size;
	}

	// MODIFY

	/** Sets a key-value pair. */
	set(key: K, value: V) {
		this._keyMap.set(key, value);
		this._valueMap.set(value, key);
	}

	/** Deletes a key-value pair from the key. */
	delete(key: K) {
		const value = this._keyMap.get(key);
		if (value !== undefined) {
			this._keyMap.delete(key);
			this._valueMap.delete(value);
		}
	}

	/** Clears all key-value pairs. */
	clear() {
		this._keyMap.clear();
		this._valueMap.clear();
	}

	// ITERATE

	/** Gets the keys iterator. */
	keys(): IterableIterator<K> {
		return this._keyMap.keys();
	}

	/** Gets the values iterator. */
	values(): IterableIterator<V> {
		return this._valueMap.keys();
	}

	/** Gets the entries iterator. */
	entries(): IterableIterator<[K, V]> {
		return this._keyMap.entries();
	}

	/** Gets the entries iterator. */
	forEach(callback: (value: V, key: K, map: Map<K, V>) => void) {
		this._keyMap.forEach(callback);
	}

	/** Gets the entries iterator. */
	[Symbol.iterator](): IterableIterator<[K, V]> {
		return this._keyMap.entries();
	}

	/** The map from keys to values. */
	private _keyMap = new Map<K, V>();

	/** The map from values to keys. */
	private _valueMap = new Map<V, K>();
}
