/** A binary data reader. */
export class Reader {

	/** Constructor. */
	constructor(data: ArrayBuffer) {
		this._dataView = new DataView(data);
		this._offset = 0;
		this._textDecoder = new TextDecoder();
	}

	/** Returns true if the offset is at the end of the data. */
	isAtEnd(): boolean {
		return this._offset >= this._dataView.byteLength;
	}

	/** Reads an 8-bit unsigned integer. */
	readUInt8(): number {
		const result = this._dataView.getUint8(this._offset);
		this._offset += 1;
		return result;
	}

	/** Reads a 16-bit unsigned integer. */
	readUInt16(): number {
		const result = this._dataView.getUint16(this._offset, true);
		this._offset += 2;
		return result;
	}

	/** Reads a 32-bit unsigned integer. */
	readUInt32(): number {
		const result = this._dataView.getUint32(this._offset, true);
		this._offset += 4;
		return result;
	}

	/** Reads a 64-bit unsigned integer. */
	readUInt64(): number {
		const result = this._dataView.getBigUint64(this._offset, true);
		this._offset += 8;
		return Number.isSafeInteger(result) ? Number(result) : NaN;
	}

	/** Reads an 8-bit integer. */
	readInt8(): number {
		const result = this._dataView.getInt8(this._offset);
		this._offset += 1;
		return result;
	}

	/** Reads a 16-bit integer. */
	readInt16(): number {
		const result = this._dataView.getInt16(this._offset, true);
		this._offset += 2;
		return result;
	}

	/** Reads a 32-bit integer. */
	readInt32(): number {
		const result = this._dataView.getInt32(this._offset, true);
		this._offset += 4;
		return result;
	}

	/** Reads a 64-bit integer. */
	readInt64(): number {
		const result = this._dataView.getBigInt64(this._offset, true);
		this._offset += 8;
		return Number.isSafeInteger(result) ? Number(result) : NaN;
	}

	/** Reads a 32-bit floating-point number. */
	readFloat32(): number {
		const result = this._dataView.getFloat32(this._offset, true);
		this._offset += 4;
		return result;
	}

	/** Reads a 64-bit floating-point number. */
	readFloat64(): number {
		const result = this._dataView.getFloat64(this._offset, true);
		this._offset += 8;
		return result;
	}

	/** Reads a null terminated UTF-8 string or a prepended u32 length string. */
	readString(endsWithZero: boolean): string {

		// Either get the length from the beginning or by finding the first zero byte.
		let length = 0;
		if (!endsWithZero) {
			length = this.readUInt32();
		}
		else {
			for (let i = this._offset; i < this._dataView.buffer.byteLength; i++) {
				if (this._dataView.getUint8(i) === 0) {
					length = i - this._offset;
					break;
				}
			}
		}

		// Do the decoding.
		const array = this._dataView.buffer.slice(this._offset, this._offset + length);
		this._offset += length;
		return this._textDecoder.decode(array);
	}

	/** Reads an array, with a prepended u32 length. */
	readArray<T>(readItemFunction: (reader: Reader) => T): T[] {
		const array: T[] = [];
		const length = this.readUInt32();
		for (let i = 0; i < length; i++) {
			array.push(readItemFunction(this));
		}
		return array;
	}

	/** Reads the remaining data as an array buffer. */
	readRest(): ArrayBuffer {
		const buffer = this._dataView.buffer.slice(this._offset);
		this._offset = this._dataView.byteLength;
		return buffer;
	}

	/** The data view for reading the data. */
	private _dataView: DataView<ArrayBuffer>;

	/** The offset in the array. */
	private _offset: number;

	/** The text encoder for writing UTF-8 strings. */
	private _textDecoder: TextDecoder;
}
