/** A lockable read/write lock. It locks in order of requests. To use it, do:
 * ```
 * const rwLock = new RwLock();
 * const unlock = await rwLock.read/write();
 * // Your code here.
 * unlock();
 * ``` */
export class RwLock {

	/** Returns a Promise that waits until there is no write lock, and then resolves to an unlock function, which releases the lock.. */
	async read(): Promise<() => void> {
		let resolve: () => void;
		const promise = new Promise<void>((_resolve) => {
			resolve = _resolve;
		});
		this.readingPromises.push(promise);
		return this.writingPromise.then(() => resolve);
	}

	/** Returns a Promise that waits until there is no write or read lock, and then resolves to an unlock function, which releases the lock. */
	async write(): Promise<() => void> {
		let resolve: () => void;
		const promise = new Promise<void>((_resolve) => {
			resolve = _resolve;
		});
		const unlock = Promise.all(this.readingPromises.concat(this.writingPromise)).then(() => resolve);
		this.readingPromises = [];
		this.writingPromise = promise;
		return unlock;
	}

	/** The latest write promise that has requested a lock (either waiting or resolved). */
	private writingPromise: Promise<void> = Promise.resolve();

	/** The latest read promises that have requested a lock (either waiting or resolved). */
	private readingPromises: Promise<void>[] = [];
}

/** For testing:

const rand = (max: number): number => Math.floor(Math.random() * max);
const delay = (ms: number): Promise<void> => new Promise(resolve => setTimeout(resolve, ms));
const lock = new RwLock();
const go = async (name: string): Promise<void> => {
	await delay(rand(50));
	const type = rand(100) < 15 ? 'write' : 'read';
	console.log(name + ' requesting ' + type + ' lock');
	const unlock = await (type === 'write' ? lock.write() : lock.read());
	console.log(name + ' got ' + type + ' lock');
	await delay(rand(1000));
	console.log(name + ' releasing ' + type + ' lock');
	unlock();
};
go('0');
go('1');
go('2');
go('3');
go('4');
go('5');
go('6');
go('7');
go('8');
go('9');

*/
