import stylistic from '@stylistic/eslint-plugin';
import stylisticTs from '@stylistic/eslint-plugin-ts';
import tsParser from '@typescript-eslint/parser';
import tsPlugin from '@typescript-eslint/eslint-plugin';

// @ts-ignore
import esPluginEsX from 'eslint-plugin-es-x';

/*
	Every possible rule for ESLint, Stylistic ESLint, TS ESLint, and Stylistic TS ESLint is present.
	If it is anything other then 'error', a comment is there to explain the difference.
*/

/** The order of members in a class. */
const memberTypes = [
	['public-instance-field', 'protected-instance-field'],
	'abstract-method',
	'constructor',
	['public-method', 'protected-method', 'private-method'],
	'private-instance-field',
	'protected-static-field',
	'private-static-field'
];

export default [

	// Don't parse these files.
	{
		ignores: ['docs/**/*', 'dist/**/*', 'node_modules/**/*']
	},

	// Parse these files.
	{
		files: ['src/**/*.js', '*.mjs', 'src/**/*.ts']
	},

	// ESLint Rules
	{
		rules: {

			// Problem rules
			'array-callback-return': 'error',
			'constructor-super': 'error',
			'for-direction': 'error',
			'getter-return': 'error',
			'no-async-promise-executor': 'error',
			// Sometimes we need awaits in loops for serial async.
			'no-await-in-loop': 'off',
			'no-class-assign': 'error',
			'no-compare-neg-zero': 'error',
			'no-cond-assign': 'error',
			'no-const-assign': 'error',
			'no-constant-binary-expression': 'error',
			'no-constant-condition': 'error',
			'no-constructor-return': 'error',
			'no-control-regex': 'error',
			'no-debugger': 'error',
			'no-dupe-args': 'error',
			// Don't need this as it is handled by TS.
			'no-dupe-class-members': 'off',
			'no-dupe-else-if': 'error',
			'no-dupe-keys': 'error',
			'no-duplicate-case': 'error',
			'no-duplicate-imports': 'error',
			'no-empty-character-class': 'error',
			'no-empty-pattern': 'error',
			'no-ex-assign': 'error',
			'no-fallthrough': 'error',
			'no-func-assign': 'error',
			'no-import-assign': 'error',
			// Don't allow function declarations anywhere but the top level.
			'no-inner-declarations': ['error', 'both', {
				blockScopedFunctions: 'disallow'
			}],
			'no-invalid-regexp': 'error',
			'no-irregular-whitespace': 'error',
			'no-loss-of-precision': 'error',
			'no-misleading-character-class': 'error',
			'no-new-native-nonconstructor': 'error',
			'no-obj-calls': 'error',
			'no-promise-executor-return': 'error',
			'no-prototype-builtins': 'error',
			'no-self-assign': 'error',
			'no-self-compare': 'error',
			'no-setter-return': 'error',
			'no-sparse-arrays': 'error',
			'no-template-curly-in-string': 'error',
			'no-this-before-super': 'error',
			// TS takes care of this one.
			'no-undef': 'off',
			'no-unexpected-multiline': 'error',
			// This gives false positives if the properties of an object are changed but not the object.
			'no-unmodified-loop-condition': 'off',
			'no-unreachable': 'error',
			'no-unreachable-loop': 'error',
			'no-unsafe-finally': 'error',
			'no-unsafe-negation': 'error',
			'no-unsafe-optional-chaining': 'error',
			'no-unused-private-class-members': 'error',
			// Turning this off in favor of the TS ESLint version.
			'no-unused-vars': 'off',
			// TS takes care of this one.
			'no-use-before-define': 'off',
			'no-useless-assignment': 'error',
			'no-useless-backreference': 'error',
			// This gives false positives on property access, so we're adjusting it.
			'require-atomic-updates': ['error', { allowProperties: true }],
			'use-isnan': 'error',
			'valid-typeof': 'error',

			// Suggestion Rules
			'accessor-pairs': 'error',
			'arrow-body-style': 'error',
			'block-scoped-var': 'error',
			'camelcase': 'error',
			// Sometimes we like lowercase comments, especially inline comments.
			'capitalized-comments': 'off',
			// This gives false positives for methods that are meant to be overridden and are empty.
			'class-methods-use-this': 'off',
			// Don't limit the complexity of functions.
			'complexity': 'off',
			// Typescript handles this.
			'consistent-return': 'off',
			// Sometimes we want to assign a variable to this, for example when walking a tree or graph.
			'consistent-this': 'off',
			'curly': 'error',
			// Many times we don't need a default case, since we have already have a default or have an exhaustive list.
			'default-case': 'off',
			'default-case-last': 'error',
			// This is better supported in TS ESLint.
			'default-param-last': 'off',
			// We turn this off because TS ESLint dot-notation can handle cases where the type is Record<string, any>.
			'dot-notation': 'off',
			'eqeqeq': 'error',
			'func-name-matching': 'error',
			'func-names': 'error',
			// Enforce no function expressions.
			'func-style': ['error', 'declaration'],
			'grouped-accessor-pairs': 'error',
			'guard-for-in': 'error',
			'id-denylist': 'error',
			// We need short variables for iterators and math.
			'id-length': 'off',
			'id-match': 'error',
			// We don't always want to initialize a declared variable if it has conditional initialization.
			'init-declarations': 'off',
			'logical-assignment-operators': 'error',
			// Don't limit the number of classes per file.
			'max-classes-per-file': 'off',
			// Don't limit the maximum depth of statements.
			'max-depth': 'off',
			// Don't limit the number of max lines per file.
			'max-lines': 'off',
			// Don't limit the number of lines per function.
			'max-lines-per-function': 'off',
			'max-nested-callbacks': 'error',
			// Don't limit the number of params of a function.
			'max-params': 'off',
			// Don't limit the number of statements per function.
			'max-statements': 'off',
			'new-cap': 'error',
			'no-alert': 'error',
			'no-array-constructor': 'error',
			'no-bitwise': 'error',
			'no-caller': 'error',
			'no-case-declarations': 'error',
			'no-console': 'error',
			// We like continue statements.
			'no-continue': 'off',
			'no-delete-var': 'error',
			'no-div-regex': 'error',
			// We sometimes like else statements after the last return to make things look more symmetrical.
			'no-else-return': 'off',
			'no-empty': 'error',
			// Overridden by TS ESLint.
			'no-empty-function': 'off',
			'no-empty-static-block': 'error',
			'no-eq-null': 'error',
			'no-eval': 'error',
			'no-extend-native': 'error',
			'no-extra-bind': 'error',
			'no-extra-boolean-cast': 'error',
			'no-extra-label': 'error',
			'no-global-assign': 'error',
			// This causes problems with things like `1 * x`, even though x is already a number.
			// There isn't a TS ESLint rule to take into account type, so we're just turning this off.
			'no-implicit-coercion': 'off',
			'no-implicit-globals': 'error',
			'no-implied-eval': 'error',
			// Sometimes we just like inline comments.
			'no-inline-comments': 'off',
			'no-invalid-this': 'error',
			'no-iterator': 'error',
			'no-label-var': 'error',
			'no-labels': 'error',
			'no-lone-blocks': 'error',
			// We sometimes like to have an if as the first statment inside an else to make things clearer.
			'no-lonely-if': 'off',
			'no-loop-func': 'error',
			// We like magic numbers everywhere! Make sure not to hard-code too many things in.
			'no-magic-numbers': 'off',
			'no-multi-assign': 'error',
			'no-multi-str': 'error',
			// We like doing `something !== undefined`, but this gives a false positive.
			'no-negated-condition': 'off',
			// Sometimes we like to nest simple ternary operations. But we still have to be careful that it doesn't get too complicated.
			'no-nested-ternary': 'off',
			'no-new': 'error',
			'no-new-func': 'error',
			'no-new-wrappers': 'error',
			'no-nonoctal-decimal-escape': 'error',
			'no-object-constructor': 'error',
			'no-octal': 'error',
			'no-octal-escape': 'error',
			// Sometimes reassigning the params is good when we want defaults or for sanitizing.
			'no-param-reassign': 'off',
			// We like using i++.
			'no-plusplus': 'off',
			'no-proto': 'error',
			// This is handled by TS.
			'no-redeclare': 'off',
			'no-regex-spaces': 'error',
			'no-restricted-exports': 'error',
			'no-restricted-globals': 'error',
			'no-restricted-imports': 'error',
			'no-restricted-properties': 'error',
			'no-restricted-syntax': 'error',
			'no-return-assign': 'error',
			'no-script-url': 'error',
			'no-sequences': 'error',
			// This can cause issues with code like: `const entity = ...; setUpdateFunction((entity) => { ... });`.
			'no-shadow': 'off',
			'no-shadow-restricted-names': 'error',
			// We like ternary operations when appopriate.
			'no-ternary': 'off',
			'no-throw-literal': 'error',
			'no-undef-init': 'error',
			// We like using undefined sometimes.
			'no-undefined': 'off',
			// We like prefixing private variables and unused params with underscores.
			'no-underscore-dangle': 'off',
			'no-unneeded-ternary': 'error',
			'no-unused-expressions': 'error',
			'no-unused-labels': 'error',
			// If we have a function named 'call', this gives a false positive.
			'no-useless-call': 'off',
			'no-useless-catch': 'error',
			'no-useless-computed-key': 'error',
			'no-useless-concat': 'error',
			'no-useless-constructor': 'error',
			'no-useless-escape': 'error',
			'no-useless-rename': 'error',
			'no-useless-return': 'error',
			'no-var': 'error',
			// To comply with TS ESLint no-floating-promises, we need to use void in promise statements.
			'no-void': ['error', { allowAsStatement: true }],
			'no-warning-comments': 'error',
			'no-with': 'error',
			'object-shorthand': 'error',
			// Enforce separate declarations for each variable.
			'one-var': ['error', 'never'],
			'operator-assignment': 'error',
			'prefer-arrow-callback': 'error',
			'prefer-const': 'error',
			// This one enforces destructuring, which can be a bit confusing.
			'prefer-destructuring': ['error', { array: false }],
			'prefer-exponentiation-operator': 'error',
			// We're fine not using named capture groups in our regexes (we'll just use $1, $2, etc).
			'prefer-named-capture-group': 'off',
			'prefer-numeric-literals': 'error',
			'prefer-object-has-own': 'error',
			'prefer-object-spread': 'error',
			'prefer-promise-reject-errors': 'error',
			'prefer-regex-literals': 'error',
			'prefer-rest-params': 'error',
			'prefer-spread': 'error',
			'prefer-template': 'error',
			// Don't enforce a radix for parseInt.
			'radix': 'off',
			// Sometimes we use async to denote a return of a promise and have no awaits.
			'require-await': 'off',
			'require-unicode-regexp': 'error',
			'require-yield': 'error',
			// We sometimes like to sort our imports in a more logical way.
			'sort-imports': ['off'],
			// We sometimes like to sort our keys in a more logical way.
			'sort-keys': 'off',
			// We don't allow multiple variables in the same declaration, except in for loops, and they may have a non-sorted order.
			'sort-vars': 'off',
			'strict': 'error',
			'symbol-description': 'error',
			'vars-on-top': 'error',
			// We sometimes like things like 0 <= index && index < 10.
			'yoda': ['error', 'never', { exceptRange: true }]
		}
	},

	// Stylistic ESLint Rules
	{
		// Use these plugins.
		plugins: {
			'@stylistic': stylistic,
			'@stylistic/ts': stylisticTs
		},
		rules: {
			// Sometimes we like newlines in arrays, sometimes not.
			'@stylistic/array-bracket-newline': 'off',
			'@stylistic/array-bracket-spacing': 'error',
			// Sometimes we like newlines in arrays, sometimes not.
			'@stylistic/array-element-newline': 'off',
			'@stylistic/arrow-parens': 'error',
			'@stylistic/arrow-spacing': 'error',
			'@stylistic/block-spacing': 'error',
			// We like the Stroustrup style of bracing, but single lines with braces on the same line are okay.
			'@stylistic/brace-style': ['error', 'stroustrup', { allowSingleLine: true }],
			'@stylistic/comma-dangle': 'error',
			'@stylistic/comma-spacing': 'error',
			'@stylistic/comma-style': 'error',
			'@stylistic/computed-property-spacing': 'error',
			'@stylistic/curly-newline': 'error',
			// If we're splitting object and property, we want the dot on the next line.
			'@stylistic/dot-location': ['error', 'property'],
			'@stylistic/eol-last': 'error',
			'@stylistic/func-call-spacing': 'error',
			// Sometimes we want the args on a single line, sometimes not, especially for longer lines.
			'@stylistic/function-call-argument-newline': 'off',
			'@stylistic/function-call-spacing': 'error',
			// We like to be flexible with where newlines are in arg lists.
			'@stylistic/function-paren-newline': 'off',
			'@stylistic/generator-star-spacing': 'error',
			// For long arrow functions, we sometimes put a break right after the arrow, which is okay.
			'@stylistic/implicit-arrow-linebreak': 'off',
			// We use tabs.
			'@stylistic/indent': ['error', 'tab', { SwitchCase: 1, tabLength: 0, ignoredNodes: ['ConditionalExpression'] }],
			'@stylistic/indent-binary-ops': ['error', 'tab'],
			// For all of these, we're not using JSX.
			'@stylistic/jsx-child-element-spacing': 'off',
			'@stylistic/jsx-closing-bracket-location': 'off',
			'@stylistic/jsx-closing-tag-location': 'off',
			'@stylistic/jsx-curly-brace-presence': 'off',
			'@stylistic/jsx-curly-newline': 'off',
			'@stylistic/jsx-curly-spacing': 'off',
			'@stylistic/jsx-equals-spacing': 'off',
			'@stylistic/jsx-first-prop-new-line': 'off',
			'@stylistic/jsx-function-call-newline': 'off',
			'@stylistic/jsx-indent': 'off',
			'@stylistic/jsx-indent-props': 'off',
			'@stylistic/jsx-max-props-per-line': 'off',
			'@stylistic/jsx-newline': 'off',
			'@stylistic/jsx-one-expression-per-line': 'off',
			'@stylistic/jsx-pascal-case': 'off',
			'@stylistic/jsx-props-no-multi-spaces': 'off',
			'@stylistic/jsx-quotes': 'off',
			'@stylistic/jsx-self-closing-comp': 'off',
			'@stylistic/jsx-sort-props': 'off',
			'@stylistic/jsx-tag-spacing': 'off',
			'@stylistic/jsx-wrap-multilines': 'off',
			'@stylistic/key-spacing': 'error',
			'@stylistic/keyword-spacing': 'error',
			// We like to put comments wherever it is suitable. More is better!
			'@stylistic/line-comment-position': 'off',
			'@stylistic/linebreak-style': 'error',
			'@stylistic/lines-around-comment': 'error',
			'@stylistic/lines-between-class-members': 'error',
			// We are limiting our lines to 160 characters, counting tabs as 2.
			'@stylistic/max-len': ['error', {
				tabWidth: 2,
				code: 160,
				ignoreRegExpLiterals: true,
				ignoreStrings: true,
				ignoreTemplateLiterals: true,
				ignoreUrls: true
			}],
			// Sometimes we like to have multiple statements on the same line, for instance math, or break statements.
			'@stylistic/max-statements-per-line': 'off',
			// If we are using semicolons, we need to enforce them to be at the end of every member.
			'@stylistic/member-delimiter-style': ['error', {
				multiline: { delimiter: 'semi', requireLast: true },
				singleline: { delimiter: 'semi', requireLast: true }
			}],
			// Sometimes we use /*, sometimes we use //.
			'@stylistic/multiline-comment-style': 'off',
			// Require new lines per part if the ternary spans multiple lines.
			'@stylistic/multiline-ternary': ['error', 'always-multiline'],
			'@stylistic/new-parens': 'error',
			// We sometimes like new lines, but sometimes it's simple enough for a single line.
			'@stylistic/newline-per-chained-call': 'off',
			'@stylistic/no-confusing-arrow': 'error',
			// Sometimes extra parens are good for clarity.
			'@stylistic/no-extra-parens': ['error', 'all', {
				allowParensAfterCommentPattern: '@type',
				enforceForArrowConditionals: false,
				nestedBinaryExpressions: false,
				ternaryOperandBinaryExpressions: false,
				nestedConditionalExpressions: false
			}],
			'@stylistic/no-extra-semi': 'error',
			'@stylistic/no-floating-decimal': 'error',
			// This enforces parens around things like 1 + x * 4, which feels excessive.
			// These options just remove the first line of arithmetic operators, which are more well known.
			'@stylistic/no-mixed-operators': ['error', { groups: [
				['&', '|', '^', '~', '<<', '>>', '>>>'],
				['==', '!=', '===', '!==', '>', '>=', '<', '<='],
				['&&', '||'],
				['in', 'instanceof']
			] }],
			'@stylistic/no-mixed-spaces-and-tabs': 'error',
			'@stylistic/no-multi-spaces': 'error',
			'@stylistic/no-multiple-empty-lines': 'error',
			// We like tabs.
			'@stylistic/no-tabs': 'off',
			'@stylistic/no-trailing-spaces': 'error',
			'@stylistic/no-whitespace-before-property': 'error',
			'@stylistic/nonblock-statement-body-position': 'error',
			'@stylistic/object-curly-newline': 'error',
			// We always want spaces between braces.
			'@stylistic/object-curly-spacing': ['error', 'always'],
			// We like flexibility in how we declare our properties in objects.
			'@stylistic/object-property-newline': 'off',
			'@stylistic/one-var-declaration-per-line': 'error',
			// When a splitting a binary op, put the binary op at the start of the next line.
			'@stylistic/operator-linebreak': ['error', 'before'],
			// We sometimes like a blank line when starting a new block for clarity.
			'@stylistic/padded-blocks': 'off',
			'@stylistic/padding-line-between-statements': 'error',
			// Quote object props consistently, only when needed.
			'@stylistic/quote-props': ['error', 'consistent-as-needed'],
			// Use single quotes, when possible.
			'@stylistic/quotes': ['error', 'single', { avoidEscape: true }],
			'@stylistic/rest-spread-spacing': 'error',
			'@stylistic/semi': 'error',
			'@stylistic/semi-spacing': 'error',
			'@stylistic/semi-style': 'error',
			'@stylistic/space-before-blocks': 'error',
			// We like spaces before arrow functions, but not named functions.
			'@stylistic/space-before-function-paren': ['error', {
				anonymous: 'always',
				named: 'never',
				asyncArrow: 'always'
			}],
			'@stylistic/space-in-parens': 'error',
			'@stylistic/space-infix-ops': 'error',
			'@stylistic/space-unary-ops': 'error',
			'@stylistic/spaced-comment': 'error',
			'@stylistic/switch-colon-spacing': 'error',
			'@stylistic/template-curly-spacing': 'error',
			'@stylistic/template-tag-spacing': 'error',
			'@stylistic/type-annotation-spacing': 'error',
			'@stylistic/type-generic-spacing': 'error',
			'@stylistic/type-named-tuple-spacing': 'error',
			'@stylistic/wrap-iife': 'error',
			'@stylistic/wrap-regex': 'error',
			'@stylistic/yield-star-spacing': 'error'
		}
	},

	// A special plugin to disable certain ES features.
	{
		plugins: {
			'es-x': esPluginEsX
		},
		rules: {
			// We aren't using accessors except in math functions. This is because we could do
			//   a.position = newPosition;
			// But you don't know if you just assigned the variable or if a setter did a copy of the variable.
			// It leads to too many uncertainties, so we're banning them, except in simple circumstances.
			'es-x/no-accessor-properties': 'error'
		}
	},

	// Things from here on are only for TS files.
	{
		files: ['src/**/*.ts'],
		languageOptions: {
			parser: tsParser,
			parserOptions: {
				ecmaFeatures: { modules: true },
				ecmaVersion: 'latest',
				projectService: './tsconfig.json'
			}
		},
		plugins: {
			'@typescript-eslint': tsPlugin,
			'@stylistic/ts': stylisticTs
		},
		rules: {

			// TS ESLint Rules
			'@typescript-eslint/adjacent-overload-signatures': 'error',
			'@typescript-eslint/array-type': 'error',
			'@typescript-eslint/await-thenable': 'error',
			'@typescript-eslint/ban-ts-comment': 'error',
			'@typescript-eslint/ban-tslint-comment': 'error',
			'@typescript-eslint/class-literal-property-style': 'error',
			// Sometimes we have class methods that don't use this, and they don't need to be static.
			'@typescript-eslint/class-methods-use-this': 'off',
			'@typescript-eslint/consistent-generic-constructors': 'error',
			'@typescript-eslint/consistent-indexed-object-style': 'error',
			// Since we use tsConfig noImplicitReturns, we don't need this on.
			'@typescript-eslint/consistent-return': 'off',
			'@typescript-eslint/consistent-type-assertions': 'error',
			// Sometimes we need to use interfaces or types, and they aren't perfectly interchangeable.
			'@typescript-eslint/consistent-type-definitions': 'off',
			'@typescript-eslint/consistent-type-exports': 'off',
			// It's good to do import { type A } from '' so that it isn't actually imported, for tree shaking.
			'@typescript-eslint/consistent-type-imports': ['error', { fixStyle: 'inline-type-imports' }],
			'@typescript-eslint/default-param-last': 'error',
			'@typescript-eslint/dot-notation': 'error',
			// We like to let TS infer the return types for us. Makes less code.
			'@typescript-eslint/explicit-function-return-type': 'off',
			// We don't want to put 'public' on all public members.
			'@typescript-eslint/explicit-member-accessibility': 'off',
			// We like to let TS infer the param and return types for us. Makes less code.
			'@typescript-eslint/explicit-module-boundary-types': 'off',
			// We don't always want to initialize a declared variable if it has conditional initialization.
			'@typescript-eslint/init-declarations': 'off',
			// We don't want ot limit the number of params.
			'@typescript-eslint/max-params': 'off',
			// There's a big list at the top of the prefered ordering of members in a class.
			'@typescript-eslint/member-ordering': ['error', {
				default: memberTypes
			}],
			// This default causes interface methods to go from 'foo(): void' to 'foo: () => void', which I don't like.
			// However it makes the functions checked contravariantly, which is better type checking.
			// See https://stackoverflow.com/a/59312254 for a detailed answer. It might be changed in a later TS.
			'@typescript-eslint/method-signature-style': 'error',
			// For variable names, it's usually anApple, but sometimes it is UI, ComponentType, or '_apple.
			'@typescript-eslint/naming-convention': ['error', {
				selector: 'default',
				format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
				leadingUnderscore: 'allow'
			}],
			'@typescript-eslint/no-array-constructor': 'error',
			'@typescript-eslint/no-array-delete': 'error',
			// We often have custom toString methods that this would give false positives on.
			'@typescript-eslint/no-base-to-string': 'off',
			'@typescript-eslint/no-confusing-non-null-assertion': 'error',
			'@typescript-eslint/no-confusing-void-expression': 'error',
			'@typescript-eslint/no-deprecated': 'error',
			'@typescript-eslint/no-dupe-class-members': 'error',
			'@typescript-eslint/no-duplicate-enum-values': 'error',
			'@typescript-eslint/no-duplicate-type-constituents': 'error',
			// We sometimes want to delete dynamic keys.
			'@typescript-eslint/no-dynamic-delete': 'off',
			// We're okay with empty functions.
			'@typescript-eslint/no-empty-function': 'off',
			// We're okay with empty interfaces.
			'@typescript-eslint/no-empty-interface': 'error',
			'@typescript-eslint/no-empty-object-type': 'error',
			'@typescript-eslint/no-explicit-any': 'error',
			'@typescript-eslint/no-extra-non-null-assertion': 'error',
			// Sometimes we like classes with all static functions to encapsulate them.
			'@typescript-eslint/no-extraneous-class': 'off',
			'@typescript-eslint/no-floating-promises': 'error',
			'@typescript-eslint/no-for-in-array': 'error',
			'@typescript-eslint/no-implied-eval': 'error',
			'@typescript-eslint/no-import-type-side-effects': 'error',
			// We prefer explicit type annotations when needed in class/interface members and named function params.
			'@typescript-eslint/no-inferrable-types': ['error', {
				ignoreParameters: true,
				ignoreProperties: true
			}],
			'@typescript-eslint/no-invalid-this': 'error',
			// This gives a false positive on a returned Promise<string | void>, which is wrong.
			'@typescript-eslint/no-invalid-void-type': 'off',
			'@typescript-eslint/no-loop-func': 'error',
			// This was deprecated.
			'@typescript-eslint/no-loss-of-precision': 'off',
			// We like magic numbers everywhere! Make sure not to hard-code too many things in.
			'@typescript-eslint/no-magic-numbers': 'off',
			'@typescript-eslint/no-meaningless-void-operator': 'error',
			'@typescript-eslint/no-misused-new': 'error',
			'@typescript-eslint/no-misused-promises': 'error',
			'@typescript-eslint/no-misused-spread': 'error',
			'@typescript-eslint/no-mixed-enums': 'error',
			// We need namespaces for when we have a class with types inside it, which isn't yet supported.
			'@typescript-eslint/no-namespace': 'off',
			'@typescript-eslint/no-non-null-asserted-nullish-coalescing': 'error',
			'@typescript-eslint/no-non-null-asserted-optional-chain': 'error',
			// We use non-null assertions often, especially with for loop lookups.
			'@typescript-eslint/no-non-null-assertion': 'off',
			'@typescript-eslint/no-redeclare': 'error',
			'@typescript-eslint/no-redundant-type-constituents': 'error',
			'@typescript-eslint/no-require-imports': 'error',
			'@typescript-eslint/no-restricted-imports': 'error',
			'@typescript-eslint/no-restricted-types': 'error',
			'@typescript-eslint/no-shadow': 'error',
			// Sometimes when walking trees or graphs we need to assign a variable to `this`.
			'@typescript-eslint/no-this-alias': 'off',
			// This was deprecated.
			'@typescript-eslint/no-type-alias': 'off',
			'@typescript-eslint/no-unnecessary-boolean-literal-compare': 'error',
			// We want to allow while (true).
			'@typescript-eslint/no-unnecessary-condition': ['error', { allowConstantLoopConditions: true }],
			'@typescript-eslint/no-unnecessary-parameter-property-assignment': 'error',
			'@typescript-eslint/no-unnecessary-qualifier': 'error',
			'@typescript-eslint/no-unnecessary-template-expression': 'error',
			'@typescript-eslint/no-unnecessary-type-arguments': 'error',
			'@typescript-eslint/no-unnecessary-type-assertion': 'error',
			'@typescript-eslint/no-unnecessary-type-constraint': 'error',
			// This gives some false positives when we want to return templates params.
			'@typescript-eslint/no-unnecessary-type-parameters': 'off',
			'@typescript-eslint/no-unsafe-argument': 'error',
			// When dealing with libraries that aren't perfectly typed, we get anys and it's okay.
			'@typescript-eslint/no-unsafe-assignment': 'off',
			'@typescript-eslint/no-unsafe-call': 'error',
			'@typescript-eslint/no-unsafe-declaration-merging': 'error',
			'@typescript-eslint/no-unsafe-enum-comparison': 'error',
			'@typescript-eslint/no-unsafe-function-type': 'error',
			'@typescript-eslint/no-unsafe-member-access': 'error',
			'@typescript-eslint/no-unsafe-return': 'error',
			// We'd like this on, but when dealing with Components and HTML, we have too many narrowings that
			// we know are right, but aren't picked up by TS. We end up fighting this rule more than it helps us.
			'@typescript-eslint/no-unsafe-type-assertion': 'off',
			'@typescript-eslint/no-unsafe-unary-minus': 'error',
			'@typescript-eslint/no-unused-expressions': 'error',
			// Underscore prefixes and things that start with a capital letter so that
			// imported classes that are only used in JSDoc work.
			'@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '^([A-Z]|_)', varsIgnorePattern: '^([A-Z]|_)' }],
			// As long as TS says it's okay, we don't need this rule.
			'@typescript-eslint/no-use-before-define': 'off',
			'@typescript-eslint/no-useless-constructor': 'error',
			'@typescript-eslint/no-useless-empty-export': 'error',
			// This was deprecated.
			'@typescript-eslint/no-var-requires': 'off',
			'@typescript-eslint/no-wrapper-object-types': 'error',
			'@typescript-eslint/non-nullable-type-assertion-style': 'error',
			'@typescript-eslint/only-throw-error': 'error',
			'@typescript-eslint/parameter-properties': 'error',
			'@typescript-eslint/prefer-as-const': 'error',
			// We don't prefer destructuring, because it can be more confusing.
			'@typescript-eslint/prefer-destructuring': 'off',
			// We don't need our enums to have number initializers.
			'@typescript-eslint/prefer-enum-initializers': 'off',
			'@typescript-eslint/prefer-find': 'error',
			// We prefer `for(let i = 0...)` loops because they are faster and reduce garbage generation.
			'@typescript-eslint/prefer-for-of': 'off',
			'@typescript-eslint/prefer-function-type': 'error',
			'@typescript-eslint/prefer-includes': 'error',
			'@typescript-eslint/prefer-literal-enum-member': 'error',
			'@typescript-eslint/prefer-namespace-keyword': 'error',
			'@typescript-eslint/prefer-nullish-coalescing': 'error',
			// This works great except when we have `if (!object: {} | null || object.text: string | null === null)`,
			// which would turn into `if (object?.text === null)`. And that fails because expression changed from
			// string | null to string | undefined | null, which isn't good. And the checkString option doesn't help.
			'@typescript-eslint/prefer-optional-chain': 'off',
			'@typescript-eslint/prefer-promise-reject-errors': 'error',
			// Although the stronger immutability is nice, all of the readonlys make the private variables harder to read.
			// We've never run into a bug where we accidentally modified a private variable from within that class.
			'@typescript-eslint/prefer-readonly': 'off',
			// This doesn't do much for param mutability because their properties can still be modified.
			// It's better to use special Readonly interfaces of classes.
			'@typescript-eslint/prefer-readonly-parameter-types': 'off',
			'@typescript-eslint/prefer-reduce-type-parameter': 'error',
			// We like using 'string'.match(regex).
			'@typescript-eslint/prefer-regexp-exec': 'off',
			'@typescript-eslint/prefer-return-this-type': 'error',
			'@typescript-eslint/prefer-string-starts-ends-with': 'error',
			// This was deprecated.
			'@typescript-eslint/prefer-ts-expect-error': 'off',
			'@typescript-eslint/promise-function-async': 'error',
			'@typescript-eslint/related-getter-setter-pairs': 'error',
			'@typescript-eslint/require-array-sort-compare': 'error',
			// We like to use async functions, but they don't always use await explicitly, as
			// some just return a promise implicitly, which is okay.
			'@typescript-eslint/require-await': 'off',
			'@typescript-eslint/restrict-plus-operands': 'error',
			// We're okay automatically turning non-strings to strings in string templates.
			'@typescript-eslint/restrict-template-expressions': 'off',
			'@typescript-eslint/return-await': 'error',
			// This was deprecated.
			'@typescript-eslint/sort-type-constituents': 'off',
			// Make sure we don't have things like `if (string | undefined)`, which can cause subtle errors.
			'@typescript-eslint/strict-boolean-expressions': ['error', { allowString: false, allowNumber: false }],
			'@typescript-eslint/switch-exhaustiveness-check': 'error',
			'@typescript-eslint/triple-slash-reference': 'error',
			// We like to let TS infer the types for us. Makes less code.
			'@typescript-eslint/typedef': 'off',
			// We often bind this to member functions that are callbacks, such as `this.foo = this.foo.bind(this)`,
			// so this gives a false positive.
			'@typescript-eslint/unbound-method': 'off',
			'@typescript-eslint/unified-signatures': 'error',
			'@typescript-eslint/use-unknown-in-catch-callback-variable': 'error',

			// Stylistic TS ESLint Rules
			'@stylistic/ts/block-spacing': 'error',
			// Using the same setting as in the JS version.
			'@stylistic/ts/brace-style': ['error', 'stroustrup', { allowSingleLine: true }],
			'@stylistic/ts/comma-dangle': 'error',
			'@stylistic/ts/comma-spacing': 'error',
			'@stylistic/ts/func-call-spacing': 'error',
			'@stylistic/ts/function-call-spacing': 'error',
			// Using the same setting as in the JS version.
			'@stylistic/ts/indent': ['error', 'tab', { SwitchCase: 1, tabLength: 0, ignoredNodes: ['ConditionalExpression'] }],
			'@stylistic/ts/key-spacing': 'error',
			'@stylistic/ts/keyword-spacing': 'error',
			'@stylistic/ts/lines-around-comment': 'error',
			'@stylistic/ts/lines-between-class-members': 'error',
			// Using the same settings as in the JS version.
			'@stylistic/ts/member-delimiter-style': ['error', {
				multiline: { delimiter: 'semi', requireLast: true },
				singleline: { delimiter: 'semi', requireLast: true }
			}],
			// Using the same settings as in the JS version.
			'@stylistic/ts/no-extra-parens': ['error', 'all', {
				allowParensAfterCommentPattern: '@type',
				enforceForArrowConditionals: false,
				nestedBinaryExpressions: false,
				ternaryOperandBinaryExpressions: false
			}],
			'@stylistic/ts/no-extra-semi': 'error',
			'@stylistic/ts/object-curly-newline': 'error',
			// Using the same settings as in the JS version.
			'@stylistic/ts/object-curly-spacing': ['error', 'always'],
			// Using the same settings as in the JS version.
			'@stylistic/ts/object-property-newline': 'off',
			'@stylistic/ts/padding-line-between-statements': 'error',
			// Using the same settings as in the JS version.
			'@stylistic/quote-props': ['error', 'consistent-as-needed'],
			// Using the same settings as in the JS version.
			'@stylistic/quotes': ['error', 'single', { avoidEscape: true }],
			'@stylistic/ts/semi': 'error',
			'@stylistic/ts/space-before-blocks': 'error',
			// Using the same settings as in the JS version.
			'@stylistic/ts/space-before-function-paren': ['error', {
				anonymous: 'always',
				named: 'never',
				asyncArrow: 'always'
			}],
			'@stylistic/ts/space-infix-ops': 'error',
			'@stylistic/ts/type-annotation-spacing': 'error'
		}
	}
];
